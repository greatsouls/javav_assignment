package com.skynetgate.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Created by Emil Gochev.
 */

@Configuration
@ComponentScan(basePackages = "com.skynetgate")
public class ClubConfig {

}
