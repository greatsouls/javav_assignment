package com.skynetgate.beans;

import org.springframework.stereotype.Component;

import java.util.Objects;

/**
 * Created by Emil Gochev.
 */

public class Statistics {

    private int numberOfGoals = 0;
    private int numberOfBookings = 0;

    public Statistics() {
    }

    public Statistics(int numberOfGoals, int numberOfBookings) {
        this.numberOfGoals = numberOfGoals;
        this.numberOfBookings = numberOfBookings;
    }

    public int getNumberOfGoals() {
        return numberOfGoals;
    }

    public void setNumberOfGoals(int numberOfGoals) {
        this.numberOfGoals = numberOfGoals;
    }

    public int getNumberOfBookings() {
        return numberOfBookings;
    }

    public void setNumberOfBookings(int numberOfBookings) {
        this.numberOfBookings = numberOfBookings;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Statistics)) return false;
        Statistics that = (Statistics) o;
        return Objects.equals(numberOfGoals, that.numberOfGoals) &&
                Objects.equals(numberOfBookings, that.numberOfBookings);
    }

    @Override
    public int hashCode() {
        return Objects.hash(numberOfGoals, numberOfBookings);
    }
}
