package com.skynetgate.beans;

import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.Locale;
import java.util.regex.Pattern;

/**
 * Created by Emil Gochev.
 */

@Component
public class FactoryPlayer {

    private String firstName;
    private String lastName;
    private int age;
    private String countryOfBirth;
    private PlayerType position;
    private BigDecimal annualSalary;
    private Currency currency;
    private Statistics statistics;
    private final Currency defaultCurrency = Currency.getInstance(Locale.US);

    public Currency getDefaultCurrency() {
        return defaultCurrency;
    }

    public Player createPlayer(String firstName, String lastName, int age, String countryOfBirth, PlayerType position,
                               BigDecimal annualSalary, Currency currency, Statistics statistics) {

        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.countryOfBirth = countryOfBirth;
        this.position = position;
        this.annualSalary = annualSalary;
        this.currency = currency;
        this.statistics = statistics;

        if (isValidPlayer()) {
            switch (position) {
                case GOALKEEPER:
                    return new PlayerGoalkeeper(this.firstName, this.lastName, this.age, this.countryOfBirth,
                            this.annualSalary, this.currency, this.statistics);
                case DEFENDER:
                    return new PlayerDefender(this.firstName, this.lastName, this.age, this.countryOfBirth,
                            this.annualSalary, this.currency, this.statistics);
                case MIDFIELDER:
                    return new PlayerMidfielder(this.firstName, this.lastName, this.age, this.countryOfBirth,
                            this.annualSalary, this.currency, this.statistics);
                case FORWARD:
                    return new PlayerForward(this.firstName, this.lastName, this.age, this.countryOfBirth,
                            this.annualSalary, this.currency, this.statistics);
            }
        }
        System.out.println("Player was NOT created");
        return null;
    }

    private boolean isValidPlayer() {
        if (isValidStr(firstName) && isValidStr(lastName) && isValidStr(countryOfBirth) &&
                isValidAge() && isValidAnnualSalary() && isValidStatistics()) {
            return true;
        } else {
            return false;
        }
    }

    private boolean isValidStr(String str) {
//validates firstName, lastName and CountryOfBirth
        final Pattern pattern = Pattern.compile("^[A-Za-z ]++$");
        if (pattern.matcher(str).matches()) {
            return true;
        } else {
            System.out.println("\nIncorrect argument for string");
            return false;
        }
    }

    private boolean isValidAge() {
        if ((age >= 20) && (age <= 23)) {
            return true;
        } else {
            System.out.println("\nIncorrect argument for age");
            return false;
        }
    }

    private boolean isValidAnnualSalary() {
        if ((!annualSalary.equals(null)) && (!annualSalary.equals(BigDecimal.ZERO)) &&
                (annualSalary.compareTo(BigDecimal.ZERO) > 0)) {
            return true;
        } else {
            System.out.println("\nIncorrect argument for salary");
            return false;
        }
    }

    private boolean isValidStatistics() {
        if ((statistics != null) && (statistics.getNumberOfGoals() >= 0) && (statistics.getNumberOfBookings() >= 0)) {
            return true;
        } else {
            System.out.println("\nIncorrect argument for Statistics");
            return false;
        }
    }
}

