package com.skynetgate.beans;

import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.Objects;

/**
 * Created by Emil Gochev.
 */

public class PlayerMidfielder extends Player{

    private String firstName;
    private String lastName;
    private int age;
    private String countryOfBirth;
    private BigDecimal annualSalary;
    private Currency currency;
    private Statistics statistics;

    public PlayerMidfielder(String firstName, String lastName, int age, String countryOfBirth,
                            BigDecimal annualSalary, Currency currency, Statistics statistics) {
        super(PlayerType.MIDFIELDER);
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.countryOfBirth = countryOfBirth;
        this.annualSalary = annualSalary;
        this.currency = currency;
        this.statistics = statistics;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getCountryOfBirth() {
        return countryOfBirth;
    }

    public void setCountryOfBirth(String countryOfBirth) {
        this.countryOfBirth = countryOfBirth;
    }

    public BigDecimal getAnnualSalary() {
        return annualSalary;
    }

    public void setAnnualSalary(BigDecimal annualSalary) {
        this.annualSalary = annualSalary;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public Statistics getStatistics() {
        return statistics;
    }

    public void setStatistics(Statistics statistics) {
        this.statistics = statistics;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PlayerMidfielder)) return false;
        PlayerMidfielder that = (PlayerMidfielder) o;
        return Objects.equals(age, that.age) &&
                Objects.equals(firstName, that.firstName) &&
                Objects.equals(lastName, that.lastName) &&
                Objects.equals(countryOfBirth, that.countryOfBirth) &&
                Objects.equals(annualSalary, that.annualSalary) &&
                Objects.equals(currency, that.currency) &&
                Objects.equals(statistics, that.statistics);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, lastName, age, countryOfBirth, annualSalary, currency, statistics);
    }

    @Override
    public String toString() {
        return "PlayerMidfielder{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", age=" + age +
                ", countryOfBirth='" + countryOfBirth + '\'' +
                ", annualSalary=" + annualSalary +
                ", currency=" + currency +
                ", statistics=" + statistics +
                '}';
    }
}
