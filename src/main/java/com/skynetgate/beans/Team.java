package com.skynetgate.beans;

import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;

/**
 * Created by Emil Gochev.
 */

public class Team {

    private List<Player> playerList;
    private Trainer trainer;
    private String name;
    private int yearOfFoundation;

    public Team(List<Player> playerList, Trainer trainer, String name, int yearOfFoundation) {
        this.playerList = playerList;
        this.trainer = trainer;
        this.name = name;
        this.yearOfFoundation = yearOfFoundation;
    }

    public List<Player> getPlayerList() {
        return playerList;
    }

    public void setPlayerList(List<Player> playerList) {
        this.playerList = playerList;
    }

    public Trainer getTrainer() {
        return trainer;
    }

    public void setTrainer(Trainer trainer) {
        this.trainer = trainer;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getYearOfFoundation() {
        return yearOfFoundation;
    }

    public void setYearOfFoundation(int yearOfFoundation) {
        this.yearOfFoundation = yearOfFoundation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Team)) return false;
        Team team = (Team) o;
        return Objects.equals(yearOfFoundation, team.yearOfFoundation) &&
                Objects.equals(playerList, team.playerList) &&
                Objects.equals(trainer, team.trainer) &&
                Objects.equals(name, team.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(playerList, trainer, name, yearOfFoundation);
    }

    @Override
    public String toString() {
        return "Team{" +
                "playerList=" + playerList +
                ", trainer=" + trainer +
                ", name='" + name + '\'' +
                ", yearOfFoundation=" + yearOfFoundation +
                '}';
    }
}
