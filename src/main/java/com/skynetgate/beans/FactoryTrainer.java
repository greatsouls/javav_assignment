package com.skynetgate.beans;

import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.regex.Pattern;

/**
 * Created by Emil Gochev.
 */

@Component
public class FactoryTrainer {

    private String firstName;
    private String lastName;
    private int age;
    private BigDecimal annualSalary;
    private Currency currency;

    public Trainer createTrainer(String firstName, String lastName, int age, BigDecimal annualSalary, Currency currency) {

        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.annualSalary = annualSalary;
        this.currency = currency;

        if (isValidTrainer()) {
            return new Trainer(this.firstName, this.lastName, this.age, this.annualSalary, this.currency);
        } else {
            System.out.println("Trainer was NOT created");
            return null;
        }
    }

    private boolean isValidTrainer() {
        if (isValidStr(firstName) && isValidStr(lastName) &&
                isValidAge() && isValidAnnualSalary()) {
            return true;
        } else {
            return false;
        }
    }

    private boolean isValidStr(String str) {
//validates firstName, lastName and CountryOfBirth
        final Pattern pattern = Pattern.compile("^[A-Za-z ]++$");
        if (pattern.matcher(str).matches()) {
            return true;
        } else {
            System.out.println("\nIncorrect argument for string");
            return false;
        }
    }

    private boolean isValidAge() {
        if (age >= 40) {
            return true;
        } else {
            System.out.println("\nIncorrect argument for age");
            return false;
        }
    }

    private boolean isValidAnnualSalary() {
        if ((!annualSalary.equals(null)) && (!annualSalary.equals(BigDecimal.ZERO)) &&
                (annualSalary.compareTo(BigDecimal.ZERO) > 0)) {
            return true;
        } else {
            System.out.println("\nIncorrect argument for salary");
            return false;
        }
    }
}
