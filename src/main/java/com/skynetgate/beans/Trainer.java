package com.skynetgate.beans;

import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.Objects;

/**
 * Created by Emil Gochev.
 */

public class Trainer {

    private String firstName;
    private String lastName;
    private int age;
    private BigDecimal annualSalary;
    private Currency currency;

    public Trainer(String firstName, String lastName, int age, BigDecimal annualSalary, Currency currency) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.annualSalary = annualSalary;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public BigDecimal getAnnualSalary() {
        return annualSalary;
    }

    public void setAnnualSalary(BigDecimal annualSalary) {
        this.annualSalary = annualSalary;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Trainer)) return false;
        Trainer trainer = (Trainer) o;
        return Objects.equals(age, trainer.age) &&
                Objects.equals(firstName, trainer.firstName) &&
                Objects.equals(lastName, trainer.lastName) &&
                Objects.equals(annualSalary, trainer.annualSalary) &&
                Objects.equals(currency, trainer.currency);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, lastName, age, annualSalary, currency);
    }

    @Override
    public String toString() {
        return "Trainer{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", age=" + age +
                ", annualSalary=" + annualSalary +
                ", currency=" + currency +
                '}';
    }
}
