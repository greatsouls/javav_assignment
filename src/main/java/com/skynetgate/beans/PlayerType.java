package com.skynetgate.beans;

/**
 * Created by Emil Gochev.
 */

public enum PlayerType {
    GOALKEEPER, DEFENDER, MIDFIELDER, FORWARD
}
