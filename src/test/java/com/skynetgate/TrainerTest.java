package com.skynetgate;

import com.skynetgate.beans.FactoryTrainer;
import com.skynetgate.beans.Trainer;
import com.skynetgate.config.ClubConfig;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.Locale;

import static org.junit.Assert.*;

/**
 * Created by Emil Gochev.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ClubConfig.class})
public class TrainerTest {

    private static String validFirstName;
    private static String validLastName;
    private static int validAge;
    private static BigDecimal validSalary;
    private static Currency currency;

    @BeforeClass
    public static void prepareData() {
        validFirstName = "First Aasd";
        validLastName = "Last";
        validAge = 42;
        validSalary = new BigDecimal("100000");
        currency = Currency.getInstance(Locale.US);
    }

    @Autowired
    private FactoryTrainer factoryTrainer;

    @Test
    public void factoryTrainerNotNull() {
        assertNotNull(factoryTrainer);
    }

    @Test
    public void createValidTrainer() {
        Trainer trainer = factoryTrainer.createTrainer(validFirstName, validLastName, validAge, validSalary, currency);
        assertNotNull(trainer);
    }

    @Test
    public void createInvalidPlayerIncorrectFirstName() {
        Trainer trainer = factoryTrainer.createTrainer("5Asd", validLastName, validAge, validSalary, currency);
        assertNull(trainer);
    }

    @Test
    public void createInvalidPlayerIncorrectLastName() {
        Trainer trainer = factoryTrainer.createTrainer(validFirstName, "At6", validAge, validSalary, currency);
        assertNull(trainer);
    }

    @Test
    public void createInvalidPlayerIncorrectAge() {
        Trainer trainer = factoryTrainer.createTrainer(validFirstName, validLastName, 19, validSalary, currency);
        assertNull(trainer);
    }

    @Test
    public void createInvalidPlayerIncorrectSalary() {
        Trainer trainer = factoryTrainer.createTrainer(validFirstName, validLastName, validAge, new BigDecimal("-123"),
                currency);
        assertNull(trainer);
    }
}
