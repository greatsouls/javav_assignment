package com.skynetgate;

import com.skynetgate.beans.FactoryPlayer;
import com.skynetgate.beans.Player;
import com.skynetgate.beans.PlayerType;
import com.skynetgate.beans.Statistics;
import com.skynetgate.config.ClubConfig;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import static org.junit.Assert.*;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.Locale;

/**
 * Created by Emil Gochev.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ClubConfig.class})
public class PlayerTest {

    private static String validFirstName;
    private static String validLastName;
    private static int validAge;
    private static String validCountryOfBirth;
    private static PlayerType position;
    private static BigDecimal validSalary;
    private static Currency currency;
    private static Statistics statistics;


    @BeforeClass
    public static void prepareData() {
        validFirstName = "First Aasd";
        validLastName = "Last";
        validAge = 22;
        validCountryOfBirth = "Canada";
        position = PlayerType.DEFENDER;
        validSalary = new BigDecimal("100000");
        currency = Currency.getInstance(Locale.US);
        statistics = new Statistics(5, 7);
    }

    @Autowired
    private FactoryPlayer factoryPlayer;

    @Test
    public void factoryPlayerNotNull() {
        assertNotNull(factoryPlayer);
    }

    @Test
    public void createValidPlayer() {
        Player player = factoryPlayer.createPlayer(validFirstName, validLastName, validAge, validCountryOfBirth,
                position, validSalary, currency, statistics);
        assertNotNull(player);
    }

    @Test
    public void createInvalidPlayerIncorrectFirstName() {
        Player player = factoryPlayer.createPlayer("5Asd", validLastName, validAge, validCountryOfBirth,
                position, validSalary, currency, statistics);
        assertNull(player);
    }

    @Test
    public void createInvalidPlayerIncorrectLastName() {
        Player player = factoryPlayer.createPlayer(validFirstName, "At6", validAge, validCountryOfBirth,
                position, validSalary, currency, statistics);
        assertNull(player);
    }

    @Test
    public void createInvalidPlayerIncorrectAge() {
        Player player = factoryPlayer.createPlayer(validFirstName, validLastName, 19, validCountryOfBirth,
                position, validSalary, currency, statistics);
        assertNull(player);
    }

    @Test
    public void createInvalidPlayerIncorrectCountry() {
        Player player = factoryPlayer.createPlayer(validFirstName, validLastName, validAge, "6Canada",
                position, validSalary, currency, statistics);
        assertNull(player);
    }

    @Test
    public void createInvalidPlayerIncorrectSalary() {
        Player player = factoryPlayer.createPlayer(validFirstName, validLastName, validAge, validCountryOfBirth,
                position, new BigDecimal("-123"), currency, statistics);
        assertNull(player);
    }

    @Test
    public void createInvalidPlayerIncorrectStatistics() {
        Player player = factoryPlayer.createPlayer(validFirstName, validLastName, validAge, validCountryOfBirth,
                position, validSalary, currency, new Statistics(-1, 5));
        assertNull(player);
    }

}
