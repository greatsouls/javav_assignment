package com.skynetgate;

import com.skynetgate.beans.*;
import com.skynetgate.config.ClubConfig;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Currency;
import java.util.List;
import java.util.Locale;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

/**
 * Created by Emil Gochev.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ClubConfig.class})
public class TeamTest {

    private static String validPlayerFirstName;
    private static String validPlayerLastName;
    private static int validPlayerAge;
    private static String validCountryOfBirth;
    private static PlayerType position;
    private static BigDecimal validPlayerSalary;
    private static Currency validPlayerCurrency;
    private static Statistics statistics;

    private static String validTrainerFirstName;
    private static String validTrainerLastName;
    private static int validTrainerAge;
    private static BigDecimal validTrainerSalary;
    private static Currency validTrainerCurrency;

    private static Player validPlayer;
    private static int validNumberOfTeamMembers;

    private static List<Player> playerList;
    private static Trainer validTrainer;
    private static String validTeamName;
    private static int validYearOfFoundation;

    @Autowired
    private FactoryPlayer factoryPlayer;

    @Autowired
    private FactoryTrainer factoryTrainer;

    @Autowired
    private FactoryTeam factoryTeam;

    @BeforeClass
    public static void prepareDataPlayer() {
        validPlayerFirstName = "First";
        validPlayerLastName = "Last";
        validPlayerAge = 22;
        validCountryOfBirth = "Canada";
        position = PlayerType.DEFENDER;
        validPlayerSalary = new BigDecimal("100000");
        validPlayerCurrency = Currency.getInstance(Locale.US);
        statistics = new Statistics(5, 7);
    }

    @BeforeClass
    public static void prepareDataTrainer() {
        validTrainerFirstName = "First Aasd";
        validTrainerLastName = "Last";
        validTrainerAge = 42;
        validTrainerSalary = new BigDecimal("100000");
        validTrainerCurrency = Currency.getInstance(Locale.US);
    }

    @BeforeClass
    public static void prepareDataTeam() {
        validTeamName = "Manchester";
        validYearOfFoundation = 1989;
        validNumberOfTeamMembers = 22;
    }

    @Test
    public void factoryTrainerNotNull() {
        assertNotNull(factoryTrainer);
    }

    @Test
    public void factoryPlayerNotNull() {
        assertNotNull(factoryPlayer);
    }

    @Test
    public void createValidTeam() {
        playerList = new ArrayList<Player>();
        validTrainer = factoryTrainer.createTrainer(validTrainerFirstName, validTrainerLastName, validTrainerAge,
                validTrainerSalary, validTrainerCurrency);
        for (int i = 1; i <= validNumberOfTeamMembers; i++) {
            validPlayer = factoryPlayer.createPlayer(validPlayerFirstName, validPlayerLastName, validPlayerAge, validCountryOfBirth,
                    position, validPlayerSalary, validPlayerCurrency, statistics);
            playerList.add(validPlayer);
        }
        Team team = factoryTeam.createTeam(playerList, validTrainer, validTeamName, validYearOfFoundation);
        assertNotNull(team);
    }

    @Test
    public void createInvalidTeamIncorrectNumberOfTeamMembers() {
        playerList = new ArrayList<Player>();
        validTrainer = factoryTrainer.createTrainer(validTrainerFirstName, validTrainerLastName, validTrainerAge,
                validTrainerSalary, validTrainerCurrency);
        for (int i = 1; i <= 25; i++) {
            validPlayer = factoryPlayer.createPlayer(validPlayerFirstName, validPlayerLastName, validPlayerAge, validCountryOfBirth,
                    position, validPlayerSalary, validPlayerCurrency, statistics);
            playerList.add(validPlayer);
        }
        System.out.println("playerList.size() is: " + playerList.size());
        Team team = factoryTeam.createTeam(playerList, validTrainer, validTeamName, validYearOfFoundation);
        assertNull(team);
    }

    @Test
    public void createInvalidTeamIncorrectTrainer() {
        playerList = new ArrayList<Player>();
        validTrainer = factoryTrainer.createTrainer("5Asd", validTrainerLastName, validTrainerAge,
                validTrainerSalary, validTrainerCurrency);
        for (int i = 1; i <= validNumberOfTeamMembers; i++) {
            validPlayer = factoryPlayer.createPlayer(validPlayerFirstName, validPlayerLastName, validPlayerAge, validCountryOfBirth,
                    position, validPlayerSalary, validPlayerCurrency, statistics);
            playerList.add(validPlayer);
        }
        Team team = factoryTeam.createTeam(playerList, validTrainer, validTeamName, validYearOfFoundation);
        assertNull(team);
    }

    @Test
    public void createInvalidTeamIncorrectName() {
        playerList = new ArrayList<Player>();
        validTrainer = factoryTrainer.createTrainer(validTrainerFirstName, validTrainerLastName, validTrainerAge,
                validTrainerSalary, validTrainerCurrency);
        for (int i = 1; i <= validNumberOfTeamMembers; i++) {
            validPlayer = factoryPlayer.createPlayer(validPlayerFirstName, validPlayerLastName, validPlayerAge, validCountryOfBirth,
                    position, validPlayerSalary, validPlayerCurrency, statistics);
            playerList.add(validPlayer);
        }
        Team team = factoryTeam.createTeam(playerList, validTrainer, "", validYearOfFoundation);
        assertNull(team);
    }

    @Test
    public void createInvalidTeamIncorrectYearOfFoundation() {
        playerList = new ArrayList<Player>();
        validTrainer = factoryTrainer.createTrainer(validTrainerFirstName, validTrainerLastName, validTrainerAge,
                validTrainerSalary, validTrainerCurrency);
        for (int i = 1; i <= validNumberOfTeamMembers; i++) {
            validPlayer = factoryPlayer.createPlayer(validPlayerFirstName, validPlayerLastName, validPlayerAge, validCountryOfBirth,
                    position, validPlayerSalary, validPlayerCurrency, statistics);
            playerList.add(validPlayer);
        }
        Team team = factoryTeam.createTeam(playerList, validTrainer, validTeamName, 1949);
        assertNull(team);
    }
}
